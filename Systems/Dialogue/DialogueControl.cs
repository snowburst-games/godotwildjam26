// this manages dialogue for the game
// can edit in godot to alter UI elements etc
// e.g. matching textures to different characters if want to show the character face

using Godot;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public class DialogueControl : Control
{
	public int DialogIndex = 0;
	public bool Finished = false;
	RichTextLabel TextLabel; //TextEdit
	RichTextLabel NameLabel; //NameDisplay
	Tween TxtTween;
	Button button;
	Button button2;
	Button button3;
	Panel panel;
	Random random = new Random();
	public List <string> MimiOne;
	public List <string> RobinOne;
	public List <string> GlennOne;
	public List <string> StanOne;
	public List <string> TobiasOne;
	public List <string> MimiTwo;
	public List <string> RobinTwo;
	public List <string> GlennTwo;
	public List <string> StanTwo;
	public List <string> TobiasTwo;	
	public List <string> CorporateDrone;
	public List<List<string>> DialogueList;
	public List<List<string>> CompletedConversationList = new List<List<string>>();
	public CharacterEnum currentCharacter;
	public List <string> Hx = new List<string>();
	private List<string> _textShownList = new List<string>();
	// the different characters that are possible
	public enum CharacterEnum { Mimi, Robin, Glenn, Stan, Tobias }


	// Publish this event if the player accuses the character (pass in the name of the character)
	// to accuse a character, e.g. when you are in convo with HR:
	// CharacterAccused?.Invoke(CharacterEnum.Jim, true);
	// this will then be picked up and e.g. a picturestory will occur showing the victory or defeat sequence
	// if the character accused is guilty, it will go to the victory sequence. otherwise the defeat sequence.
	public delegate void CharacterAccusedDelegate(CharacterEnum character, bool guilty);
	public event CharacterAccusedDelegate CharacterAccused;

	// Publish this event after a dialogue with the character with an appropriate string
	// a string entry is then added to the player's journal
	// to update the journal:
	// JournalUpdated?.Invoke("test entry");
	public delegate void JournalUpdatedDelegate(string journalUpdated);
	public event JournalUpdatedDelegate JournalUpdated;

	// when it is appropriate for the companion to say something, publish this event
	// usage:
	// CompanionReacts?.Invoke("snarky comment");
	public delegate void CompanionReactsDelegate(string speech);
	public event CompanionReactsDelegate CompanionReacts;
	
	
	public override void _Ready()
	{
		GetNodes(); 
		LoadLongDialog(); //AnimateText
		MakeDialogues();
		button3.Text = "BYE!";
		panel.Hide();
		//OnTalkToPam();
	}
/////////////////////////REMEMBER TO DELETE/////////////////////

private void OnMimiPressed()
{
	Start(CharacterEnum.Mimi);
}


private void OnRobinPressed()
{
	Start(CharacterEnum.Robin);
}


private void OnTobiasPressed()
{
	Start(CharacterEnum.Tobias);
}


private void OnGlennPressed()
{
	Start(CharacterEnum.Glenn);
}


private void OnStanPressed()
{
	Start(CharacterEnum.Stan);
}

	// private void OnTalkToPam() //testing
	// {
	// 	Start(CharacterEnum.Pam);
	// }

	// private void OnTalkToDwight() //testing
	// {
	// 	Start(CharacterEnum.Dwight);
	// }
//////////////////////////////////////////////////////////////
	// this commences the dialogue
	// the different characters are above in CharacterEnum - can add characters as needed
	public void Start(CharacterEnum character)
	{
		currentCharacter = character;
		panel.Show();
		button.Show();
		button2.Show();
		NameLabel.Text = NameDict[character];
		LoadLongDialog();
		DisplayConversation(character, ConversationManager(currentCharacter));	
		GD.Print("commencing dialogue with: ", character.ToString());
	}


	// called when this scene is disposed
	public void Die()
	{
		CharacterAccused = null;
		JournalUpdated = null;
		CompanionReacts = null;
	}

	public void MakeDialogues()
	{
		DialogueList = new List<List<string>>()
		{
			{GlennOne = new List<string>()
				{"Mmm...  broccoli brownie - want one? It's good for you, you know.",
				"I’m looking for my lunch. Did you eat my sandwich?",
				"Glenn, what were you doing between 12:00 and 13:00?",
				"Eat your shop-bought food when I have this delicious home-made treat? Never!",
				"Well... I was in the toilet the entire time. I must add more fibre to this recipe.",
				"Glenn:One",
				null}},
			{RobinOne = new List<string>()
				{"... Me? You want to talk to me? No-one ever talks to me.",
				"Did you see anyone leave the office today?",
				"Robin, where were you between 12:00 and 1:00pm?",
				"N... no. But Glenn locked himself in the toilet for a whole hour at midday. That was bizarre.",
				"Here. I haven’t left my seat. In early, out late... yet no-one even looks at me. Strange.",
				"Robin:One",
				null}},
			{StanOne = new List<string>()
				{"There. I've labelled everything. Try to pinch my property now, “team”...",
				"You've even labelled your water bottle. What happened?",
				"My sandwich was stolen from the kitchen - did you eat it?",
				"We have a thief amoung us! First my stapler, then my beloved cat pictures. Must. Label. Everything.",
				"I'm offended by the mere suggestion. And I like to stay as stationary as possible. I haven't moved since 9:00am.",
				"Stan:One",
				null}},
			{TobiasOne = new List<string>()
				{"Oh. You. Four colleagues downstairs and no-one comes to see me.", //I don't get many visitors up here.
				"My lunch is missing. What were you doing between 12:00-13:00?",
				"Four? You said four colleagues. There are five of us downstairs.",
				"Well... I was here, alone. In a meeting. Now I’m having a late lunch of... this bowl of nuts!",
				"No, I don't think so. There are 5 of us in the office. We still have that noob vacancy...",
				"Tobias:One",
				null}},
			{MimiOne = new List<string>()
				{"Happy Halloween! Oh... You look enraged. What's the matter?",
				"Mimi, have you seen my sandwich?",
				"Mimi, where were you between 12:00 and 1:00pm?",
				"Haha! You are a funny one... Oh, you're serious. No.",
				"I... I popped out for a Ploughman's at the local. Needed some alone time.",
				"Mimi:One",
				null}},
			{CorporateDrone = new List<string>(){"I'm in the middle of some blue sky thinking - come back later...", "Time is money! Let's circle back to this another time!", "Talk to me later. Got to hit the ground running!", "I'm busy, touching base and moving forward. ", "Curiousity killed the cat. Must fly!", null, null}},

			{StanTwo = new List<string>()
				{"It's missing. Thieves! One of you stole my pen!",
				"Do you suspect ROBIN, THE NOOB? She's a bit odd.",
				"Who is the thief?",
				"Who? I haven't met Robin. Haven't seen a new face in years.",
				"Mimi has been rather suspicious lately. She lied to you about lunch, you know… ",
				"Stan:Two",
				"Stan:One"}},//Condition: All the first round conversations.
			{MimiTwo = new List<string>()
				{"Stan's awfully paranoid, isn’t he? Don't believe a word he says.",
				"Stan said you lied about where you were at lunch.",
				"Have you seen anything suspicious lately?",
				"Okay. I told a little lie. I had lunch with Tobias in his office, but I can't tell you why...",
				"Glenn's broccoli brownies... he threw a whole batch of them in the bin! What's up with that?",
				"Mimi:Two",
				"Mimi:One"}},//Condition: All the first round conversations plus second round STAN
			{GlennTwo = new List<string>()
				{"You again? What do you want now?",
				"Why did you throw away your home-made brownies!?",
				"You ate my sandwich, didn't you?",
				"Are you surprised? Never put broccoli in your brownie recipe.",
				"I'm ashamed to say I tried. But when I looked for it at around 1:00pm, it wasn't there, I swear!",
				"Glenn:Two", //Condition: All the first round conversations plus second round MIMI
				"Glenn:One"}},	
			{RobinTwo = new List<string>()
				{"Hello again! Two chats in one day - what a treat!",
				"I have a question for you. Why doesn't anyone know who you are?",
				"Things have been disappearing since you turned up. Are you the thief?",
				"They're an unfriendly bunch, I suppose. Tobias certainly doesn't like you.",
				"No... Of course not! I think it might be Glenn. He seems suspicious... ",
				"Robin:Two",
				"Robin:One"}},//All the first round conversations
			{TobiasTwo = new List<string>()
				{"You again? What do you want now? I haven't taken anything of yours",
				"What's the beef, Tobias? Do you have a problem with me?",
				"Do you hate me enough to eat my sandwich?",
				"You heard about that? Sorry. I'm a sore loser... You thrashed me at Tic Tac Toe, and I’ve never gotten over it.",
				"It wasn't me. If you don't believe me, ask Mimi.",
				"Tobias:Two",
				"Tobias:One"}},//All the first round conversations + Robin second round.
		};	
	}


	public Dictionary<CharacterEnum, string> NameDict = new Dictionary<CharacterEnum, string>()
	{
		{CharacterEnum.Glenn, "GLENN, THE ASSISTANT MANAGER"}, //should each character have their own dictionary of choices:responses
		{CharacterEnum.Mimi, "MIMI, THE MANAGER"},
		{CharacterEnum.Robin, "ROBIN, THE NOOB"},
		{CharacterEnum.Stan, "STAN, THE TECH GUY"},
		{CharacterEnum.Tobias, "TOBIAS, HR"},
	};

	public void AddConversationToHistory(CharacterEnum currentChar, List<string> list)
	{
		Hx.Add(list[5]);
		if (list == MimiOne)
		{
			JournalUpdated?.Invoke("Mimi says: She was having lunch outside at the time of the crime.");
		}
		if (list == MimiTwo)
		{
			JournalUpdated?.Invoke("Mimi says: She lied about having lunch outside at the time of the crime. She was with Tobias");
		}
		if (list == GlennOne)
		{
			JournalUpdated?.Invoke("Glenn says: He was in the toilet at the time of the crime. For one hour.");
		}
		if (list == GlennTwo)
		{
			JournalUpdated?.Invoke("Glenn says: He tried to steal my sandwich at 13:00pm, but it was already gone");
		}
		if (list == RobinOne)
		{
			JournalUpdated?.Invoke("Robin says: Glenn was in the toilet between 12:00-13:00. She was happy that I spoke to her.");
		}
		if (list == RobinTwo)
		{
			JournalUpdated?.Invoke("Robin says: Tobias doesn't like me. She thinks Glenn is suspicious.");
		}
		if (list == StanOne)
		{
			JournalUpdated?.Invoke("Stan says: Someone in the office has been stealing his things.");
		}
		if (list == StanTwo)
		{
			JournalUpdated?.Invoke("Stan says: Mimi lied to me about her whereabouts at lunch.");
		}
		if (list == TobiasOne)
		{
			JournalUpdated?.Invoke("Tobias says: He was in a meeting, alone, at the time of the crime.");
		}
		if (list == TobiasTwo)
		{
			JournalUpdated?.Invoke("Tobias says: He was with Mimi at the time of the crime. He is also jealous of my talent.");
		}
	}
	void GetNodes()
	{
		panel = (Panel)GetNode("CanvasLayer/Panel");
		TextLabel = (RichTextLabel)GetNode("CanvasLayer/Panel/TextLabel");
		TxtTween = (Tween)GetNode("CanvasLayer/Panel/TxtTween");
		NameLabel = (RichTextLabel)GetNode("CanvasLayer/Panel/NameLabel");
		button = (Button)GetNode("CanvasLayer/Panel/DialogResponse/Button");
		button2 = (Button)GetNode("CanvasLayer/Panel/DialogResponse/Button2");
		button3 = (Button)GetNode("CanvasLayer/Panel/DialogResponse/Button3");
	}
	public void LoadLongDialog()
	{
			Finished = false;
			TextLabel.PercentVisible = 0;
			TxtTween.InterpolateProperty(TextLabel,"percent_visible", 0, 1, 1, Tween.TransitionType.Linear, Tween.EaseType.InOut);
			TxtTween.Start();
			DialogIndex += 1;	
	}
	private void OnButtonPressed() //Show reply 1 and hide button 1
	{
		ShowReplyOne(currentCharacter, ConversationManager(currentCharacter));	
	}

	public void ShowReplyOne(CharacterEnum characEnum, List<string> list)
	{
		LoadLongDialog();
		TextLabel.Text = list[3];
		button.Hide();
		_textShownList.Add(list[1]);
				
		if (_textShownList.Contains(list[1]) &&_textShownList.Contains(list[2]))
		{
			BanishConvo(currentCharacter, ConversationManager(currentCharacter));
		}
	}
	private void OnButton2Pressed()
	{
		ShowReplyTwo(currentCharacter, ConversationManager(currentCharacter));
	}

	private void ShowReplyTwo(CharacterEnum characEnum,  List<string> list)
	{
		LoadLongDialog();
		TextLabel.Text = list[4];
		button2.Hide();	
		_textShownList.Add(list[2]);
		
		if (_textShownList.Contains(list[1]) && _textShownList.Contains(list[2]))
		{
			BanishConvo(currentCharacter, ConversationManager(currentCharacter));
		}
	}
	private void OnButton3Pressed()
	{

		panel.Hide();
		// BanishConvo(currentCharacter, ConversationManager(currentCharacter));
	}

	private void BanishConvo(CharacterEnum characEnum,  List<string> list)
	{	
		CompletedConversationList.Add(list);
		AddConversationToHistory(characEnum, list);
	}
	private bool IsConvoBanished(CharacterEnum characEnum, List<string> list)
	{
		if (CompletedConversationList.Contains(list))
		{
			GD.Print("true");
			return true;
		}
		else
		{
			GD.Print("false");
			return false;
		}
		
	}

	public void DisplayConversation(CharacterEnum characEnum, List<string> list)
	{
		
		if	(IsConvoBanished(characEnum, list))
			{
				int n;
				n = random.Next(0, 5);
				TextLabel.Text = CorporateDrone[n];
				//TextLabel.Text = "Time is money! Let's circle back to this another time!";
				button.Hide();
				button2.Hide();
			}
		if (!IsConvoBanished(characEnum, list))
			{
				if (list == CorporateDrone)
				{
					int n;
					n = random.Next(0, 5);
					TextLabel.Text = list[n];
					button.Hide();
					button2.Hide();
				}
				
				if (list[1] != null && list != CorporateDrone) 
					{
						if (_textShownList.Contains(list[1]))
						{
							// GD.Print("ALREADY SEEN THIS!");
							button.Visible = false;
						}
						else
						{
							TextLabel.Text = list[0];
							button.Show();
							button.Text = list[1];
						}
					}
				if (list[2] != null && list != CorporateDrone)
					{
						if (_textShownList.Contains(list[2]))
						{
							// GD.Print("ALREADY SEEN THIS!");
							button2.Visible = false;
						}
						else
						{
							TextLabel.Text = list[0];
							button2.Show();
							button2.Text = list[2];
						}
					}

			
				// else
				// {
				// 	button.Hide();
				// 	button2.Hide();
				// }
		}
					
	}


	public List<string> ConversationManager(CharacterEnum characEnum)
	{
		//GD.Print(currentCharacter + "in convo manager");
		string name = NameDict[characEnum];
		//GD.Print(name + "name of current charac");



		if ((name == "GLENN, THE ASSISTANT MANAGER") && (Hx.Contains("Glenn:One")) && (Hx.Contains("Mimi:Two")) && (Hx.Contains("Mimi:One")) && (Hx.Contains("Robin:One")) && (Hx.Contains("Stan:One")) && (Hx.Contains("Tobias:One")))
		{
			GD.Print("Glenn:Two returned");
			return GlennTwo;
		}
		if ( (name == "MIMI, THE MANAGER") && (Hx.Contains("Glenn:One")) && (Hx.Contains("Mimi:One")) && (Hx.Contains("Robin:One")) && (Hx.Contains("Stan:Two")) && (Hx.Contains("Stan:One")) && (Hx.Contains("Tobias:One")))
		{
			GD.Print("Mimi:Two returned");
			return MimiTwo;
		}
		if ((name == "ROBIN, THE NOOB") && (Hx.Contains("Glenn:One")) && (Hx.Contains("Mimi:One")) && (Hx.Contains("Robin:One")) && (Hx.Contains("Stan:One")) && (Hx.Contains("Tobias:One")))
		{
			GD.Print("Robin:Two returned");
			return RobinTwo;
		}
		if ((name == "STAN, THE TECH GUY") && (Hx.Contains("Glenn:One")) && (Hx.Contains("Mimi:One")) && (Hx.Contains("Robin:One")) && (Hx.Contains("Stan:One")) && (Hx.Contains("Tobias:One")))
		{
			GD.Print("Stan:two returned");
			return StanTwo;
		}
		if ((name == "TOBIAS, HR") && (Hx.Contains("Glenn:One")) && (Hx.Contains("Mimi:One")) && (Hx.Contains("Robin:Two")) && (Hx.Contains("Robin:One")) && (Hx.Contains("Stan:One")) && (Hx.Contains("Tobias:One")))
		{
			GD.Print("Tobias:two returned");
			return TobiasTwo;
		}
		///////////////////////////////might need to remove the !hx
		if ((!Hx.Contains("Glenn:One")) && (name == "GLENN, THE ASSISTANT MANAGER"))
		{
			GD.Print("Glenn:One returned");
			return GlennOne;
		}
		if ((!Hx.Contains("Mimi:One")) && (name == "MIMI, THE MANAGER"))
		{
			GD.Print("Mimi:One returned");
			return MimiOne;
		}
		if ((!Hx.Contains("Robin:One")) && (name == "ROBIN, THE NOOB"))
		{
			GD.Print("Robin:One returned");
			return RobinOne;
		}
		if ((!Hx.Contains("Stan:One")) && (name == "STAN, THE TECH GUY"))
		{
			GD.Print("Stan:One returned");
			return StanOne;
		}
		if ((!Hx.Contains("Tobias:One")) && (name == "TOBIAS, HR"))
		{
			GD.Print("Stan:One returned");
			return TobiasOne;
		}
		

		else
			{
				GD.Print("corporate drone list returned");	
				return CorporateDrone;	
			}
			
	}
	/*
		{CharacterEnum.Glen, "Glen, the assistant manager"}, //should each character have their own dictionary of choices:responses
		{CharacterEnum.Mimi, "MIMI, THE MANAGER"},
		{CharacterEnum.Robin, "ROBIN, THE NOOB"},
		{CharacterEnum.Stan, "STAN, THE TECH GUY"},
		{CharacterEnum.Tobias, "TOBIAS, HR"},
	*/
	private void OnTxtTweenCompleted(Godot.Object @object, NodePath key)
	{
		Finished = true;
	}

	void LoadConvo(int convoID)
	{
		panel.Show();
	}

	// public void HideDialogueWindow()
	// {
	// 	panel.Hide();
	// }


}



