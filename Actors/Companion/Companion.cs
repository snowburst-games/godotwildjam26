using Godot;
using System;
using System.Collections.Generic;

public class Companion : Node2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// private Sprite _spriteSpeechBubble;
	private Label _lblBubble;
	private AnimationPlayer _anim;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.CompanionSounds, AudioStream> _companionSounds = AudioData.LoadedSounds<AudioData.CompanionSounds>(AudioData.CompanionSoundPaths);

	private Dictionary<string, AudioData.CompanionSounds> _textSoundDict = new Dictionary<string, AudioData.CompanionSounds>()
	{
		{"test", AudioData.CompanionSounds.Test1}
	};


	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_lblBubble = GetNode<Label>("SpriteSpeechBubble/LblBubble");
		_anim = GetNode<AnimationPlayer>("Anim");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		// _spriteSpeechBubble = GetNode<Sprite>("SpriteSpeechBubble");
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }

	public void StartSpeech(string text)
	{
		_lblBubble.Text = text;
		_anim.Stop();
		_anim.Play("FadeInOut");
		AudioData.CompanionSounds sound = _textSoundDict["test"];
		AudioHandler.PlaySound(_soundPlayer, _companionSounds[sound], AudioData.SoundBus.Voice);		
	}


}
