using Godot;
using System;
using System.Collections.Generic;

public class BtnBase : TextureButton
{
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.ButtonSounds, AudioStream> _buttonSounds = AudioData.LoadedSounds<AudioData.ButtonSounds>(AudioData.ButtonSoundPaths);

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
	}

	private void OnBtnBaseButtonDown()
	{
		AudioHandler.PlaySound(_soundPlayer, _buttonSounds[AudioData.ButtonSounds.ClickStart], AudioData.SoundBus.Effects);
	}
}
