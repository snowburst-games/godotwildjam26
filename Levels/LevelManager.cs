using Godot;
using System;
using System.Collections.Generic;

public class LevelManager : Control
{
	public enum Level { Office, Kitchen, HR, Toilet }
	private Dictionary<Level, SceneData.Scene> _roomsDict = new Dictionary<Level, SceneData.Scene>() {
		{Level.Office, SceneData.Scene.RoomOffice},
		{Level.Kitchen, SceneData.Scene.RoomKitchen},
	};

	private AnimationPlayer _anim;
	private TextureRect _texFade;

	[Export]
	private Level _startingLevel = Level.Office;

	private Room _currentLevel;

	private DialogueControl _dialogueControl;

	public override void _Ready()
	{
		_anim = GetNode<AnimationPlayer>("Anim");
		_dialogueControl = GetNode<DialogueControl>("DialogueControl");
		_texFade = GetNode<TextureRect>("CanvasLayer/TexFade");
		_texFade.Visible = false;

		SetLevel(_startingLevel);
	}

	public async void SetLevelWithFade(Level level)
	{
		if (!_roomsDict.ContainsKey(level))
		{
			return;
		}
		_anim.Play("FadeOut");
		await ToSignal(_anim, "animation_finished");
		SetLevel(level);
		_anim.Play("FadeIn");
	}

	public void SetLevel(Level level)
	{
		if (_currentLevel != null)
		{
			_currentLevel.Die();
		}
		PackedScene _levelScn = GD.Load<PackedScene>(SceneData.Scenes[_roomsDict[level]]);
		_currentLevel = (Room) _levelScn.Instance();
		_currentLevel.Init();
		_currentLevel.DoorClicked+=this.SetLevelWithFade;
		AddChild(_currentLevel);
		_currentLevel.CharacterManager.CharacterClicked+=_dialogueControl.Start;
	}
}
