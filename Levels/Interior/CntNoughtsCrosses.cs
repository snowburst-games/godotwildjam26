using Godot;
using System;
using System.Collections.Generic;

public class CntNoughtsCrosses : Control
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private NoughtsCrossesGame _noughts;
	private AnimationPlayer _anim;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.NoughtsSounds, AudioStream> _noughtsSounds = AudioData.LoadedSounds<AudioData.NoughtsSounds>(AudioData.NoughtsSoundPaths);

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_noughts = GetNode<NoughtsCrossesGame>("NoughtsCrossesGame");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("NoughtsCrossesGame/SoundPlayer");
		_anim = GetNode<AnimationPlayer>("NoughtsCrossesGame/AnimStart");
		_noughts.Visible = false;
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	private void OnBtnNoughtsCrossesPressed()
	{
		if (_noughts.Visible)
		{
			return;
		}
		_noughts.Reset();
		_anim.Play("Appear");
		AudioHandler.PlaySound(_soundPlayer, _noughtsSounds[AudioData.NoughtsSounds.Start], AudioData.SoundBus.Effects);
	}

}

