using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class NoughtsCrossesGame : Control
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Panel _panel;
	private Label _lblInfo;
	private Label _lblOpponent;
	private LineEdit _cheat;
	private AnimationPlayer _anim;
	private CPUParticles2D _particlesBread;
	private AudioStreamPlayer2D _soundPlayer;
	private Timer _fireworksTimer;
	private Dictionary<AudioData.NoughtsSounds, AudioStream> _noughtsSounds = AudioData.LoadedSounds<AudioData.NoughtsSounds>(AudioData.NoughtsSoundPaths);

	// private int[int[]] _board;// = new int[3] {0,0,0};
	private int[,] _board = new int[3,3];

	private Dictionary<string, Tuple<int, int>> _btnBoardDict;
	private Random _rand = new Random();
	private int _computerPlayer;
	private int _humanPlayer;
	private int _currentTurn = 2;

	private enum DifficultyEnum { Easy, Medium, Hard}
	private DifficultyEnum _difficulty = DifficultyEnum.Easy;
	private Dictionary<DifficultyEnum, Action> _difficultyAIMethodDict;

	private DialogueControl.CharacterEnum _opponent = DialogueControl.CharacterEnum.Glenn;

	private Dictionary<DialogueControl.CharacterEnum, DifficultyEnum> _characterDifficulties = new Dictionary<DialogueControl.CharacterEnum, DifficultyEnum>()
	{
		{DialogueControl.CharacterEnum.Glenn, DifficultyEnum.Easy},
		{DialogueControl.CharacterEnum.Mimi, DifficultyEnum.Hard},
		{DialogueControl.CharacterEnum.Robin, DifficultyEnum.Easy},
		{DialogueControl.CharacterEnum.Stan, DifficultyEnum.Medium},
		{DialogueControl.CharacterEnum.Tobias, DifficultyEnum.Medium},
	};

	private Action _AIMethod;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_panel = GetNode<Panel>("Panel");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_lblInfo = GetNode<Label>("PnlInfo/Label");
		_lblOpponent = GetNode<Label>("PnlOpponent/Label");
		_cheat = GetNode<LineEdit>("Cheat");
		_anim = GetNode<AnimationPlayer>("Anim");
		_particlesBread = GetNode<CPUParticles2D>("ParticlesBread");
		_fireworksTimer = GetNode<Timer>("FireworksTimer");
		foreach (Node n in _panel.GetChildren())
		{
			if (n is Button button)
			{
				button.Connect("pressed", this, nameof(OnButtonPressed), new Godot.Collections.Array {button.Name});
			}
		}

		_difficultyAIMethodDict = new Dictionary<DifficultyEnum, Action>() {
			{DifficultyEnum.Easy, AIRandomMove},
			{DifficultyEnum.Medium, AIDumbMove},
			{DifficultyEnum.Hard, AIBestMove}
		};

		_btnBoardDict = new Dictionary<string, Tuple<int, int>>()
		{
			{"Btn00", new Tuple<int, int>(0,0)},
			{"Btn01", new Tuple<int, int>(0,1)},
			{"Btn02", new Tuple<int, int>(0,2)},
			{"Btn03", new Tuple<int, int>(1,0)},
			{"Btn04", new Tuple<int, int>(1,1)},
			{"Btn05", new Tuple<int, int>(1,2)},
			{"Btn06", new Tuple<int, int>(2,0)},
			{"Btn07", new Tuple<int, int>(2,1)},
			{"Btn08", new Tuple<int, int>(2,2)}
		};
		Reset();
	}

	public override void _Input(InputEvent ev)
	{
		base._Input(ev);
		if (ev is InputEventKey evk && ! ev.IsEcho() && ev.IsPressed())
		{
			if (Input.IsKeyPressed((int)KeyList.Quoteleft) || Input.IsKeyPressed((int)KeyList.Asciitilde))
			{
				_cheat.Visible = ! _cheat.Visible;
				_cheat.Text = "";
			}
			if (_cheat.Visible)
			{
				string input = OS.GetScancodeString(evk.Scancode);
				string output = string.Empty;
				for (int i = 0; i < input.Length; i++)
				{
					if (Char.IsDigit(input[i]))
					{
						output += input[i];
					}
				}
				_cheat.Text += output;

				if (_cheat.Text == "32167")
				{
					OnCheat();
				}
			}
		}
	}

	public void OnCheat()
	{

		for (int i = 0; i <= _board.GetUpperBound(0); i++)
		{
			for (int j = 0; j <= _board.GetUpperBound(1); j++)
			{
				SetBoardValue(i, j, _humanPlayer);
				SetButtonText(_btnBoardDict.ToDictionary(x => x.Value, x => x.Key)[new Tuple<int,int>(i,j)], _humanPlayer);
				_lblInfo.Text = "Is it really winning when you cheat?";
				_currentTurn = 0;
				_particlesBread.Emitting = true;
				_fireworksTimer.Start();
				AudioHandler.PlaySound(_soundPlayer, _noughtsSounds[AudioData.NoughtsSounds.CheatVictory], AudioData.SoundBus.Effects);
				// NextTurn();
			}
		}
		// _anim.Play("Flip");
		// await ToSignal(_anim, "animation_finished");
		// OnBtnClosePressed();
	}

	

	public void Reset()
	{
		_currentTurn = 2;
		_cheat.Visible = false;
		_particlesBread.Emitting = false;
		RectRotation = 0;
		RectPosition = new Vector2(0,0);
		RectScale = new Vector2(1,1);
		_opponent = ((DialogueControl.CharacterEnum) _rand.Next(0,Enum.GetNames(typeof(DialogueControl.CharacterEnum)).Length));
		_lblOpponent.Text = "Your opponent is... " + _opponent.ToString() + "!";
		_difficulty = _characterDifficulties[_opponent];
		_AIMethod = _difficultyAIMethodDict[_difficulty];
		_computerPlayer = _rand.Next(1,3);
		_humanPlayer = _computerPlayer == 1 ? 2 : 1;
		foreach (Node n in _panel.GetChildren())
		{
			if (n is Button button)
			{
				button.GetNode<Label>("Label").Text = "";
			}
		}
		for (int i = 0; i <= _board.GetUpperBound(0); i++)
		{
			for (int j = 0; j <= _board.GetUpperBound(1); j++)
			{
				_board[i, j] = 0;
			}
		}
		NextTurn();
	}

	public void NextTurn()
	{
		if (IsWinner() != 0)
		{
			DeclareWinner(IsWinner());
			return;
		}
		if (IsStalemate())
		{
			DeclareWinner(0);
			return;
		}

		_currentTurn = _currentTurn == 1 ? 2 : 1;
		if (_currentTurn == _computerPlayer)
		{
			_lblInfo.Text = "You are " + (_humanPlayer == 1 ? "x" : "o") + ".\nOpponent's turn...";
			DoAITurn();
			return;
		}
		_lblInfo.Text = "You are " + (_humanPlayer == 1 ? "x" : "o") + ".\nYour turn!";
		
	}

	public void DeclareWinner(int winner)
	{
		_lblInfo.Text = winner == _computerPlayer ? "You lose!" : winner == 0 ? "It's a draw..." : "You win!";
		_currentTurn = 0;
	}

	public async void DoAITurn()
	{
		await Task.Run(_AIMethod);
		NextTurn();
	}


	public void AIFirstMove()
	{
		for (int i = 0; i <= _board.GetUpperBound(0); i++)
		{
			for (int j = 0; j <= _board.GetUpperBound(1); j++)
			{
				if (_board[i, j] == 0)
				{
					SetAIMove(i, j);
					return;
				}
			}
		}
	}

	public void AIRandomMove()
	{
		int x = _rand.Next(0,3);
		int y = _rand.Next(0,3);
		if (_board[x, y] == 0)
		{
			SetAIMove(x, y);
		}
		else
		{
			AIRandomMove();
		}
	}

	public void AIBestMove()
	{


		// await ToSignal (GetTree(), "idle_frame");
		int bestScore = -1000;
		int[] move = new int[2] {-1, -1};
		for (int i = 0; i <= _board.GetUpperBound(0); i++)
		{
			for (int j = 0; j <= _board.GetUpperBound(1); j++)
			{
				if (_board[i, j] == 0)
				{
					_board[i, j] = _computerPlayer;
					int score = MiniMax(_board, 0, false);
					_board[i, j] = 0;
					if (score > bestScore)
					{
						bestScore = score;
						move = new int[2] {i, j};
					}
				}
			}
		}
		if (!(move[0] == -1 || move[1] == -1))
		{
			SetAIMove(move[0], move[1]);
		}
		else
		{
			GD.Print("there were no moves available for minimax?");
		}
	}

	public void AIDumbMove()
	{

		int[] move = new int[2] {-1, -1};
		for (int i = 0; i <= _board.GetUpperBound(0); i++)
		{
			for (int j = 0; j <= _board.GetUpperBound(1); j++)
			{
				if (_board[i, j] == 0)
				{
					_board[i, j] = _computerPlayer;
					
					if (IsWinner() == _computerPlayer)
					{
						move = new int[2] {i, j};
						_board[i, j] = 0;
						break;
					}
					_board[i, j] = _humanPlayer;
					if (IsWinner() == _humanPlayer)
					{
						move = new int[2] {i, j};
						_board[i, j] = 0;
						break;
					}
					_board[i, j] = 0;
				}
			}
		}
		if (!(move[0] == -1 || move[1] == -1))
		{
			GD.Print(move[0],", ", move[1]);
			SetAIMove(move[0], move[1]);
		}
		else
		{
			AIRandomMove();
		}
	}

	public int MiniMax(int[,] _board, int depth, bool isMaximising)
	{

		int score = 0;
		int result = IsWinner() == _computerPlayer ? 1 : IsWinner() == _humanPlayer ? -1 : IsStalemate() ? 0 : -1000;
		if (result != -1000)
		{
			return result;
		}

		if (isMaximising)
		{
			int bestScore = -1000;
			for (int i = 0; i <= _board.GetUpperBound(0); i++)
			{
				for (int j = 0; j <= _board.GetUpperBound(1); j++)
				{
					if (_board[i, j] == 0)
					{
						_board[i, j] = _computerPlayer;
						score = MiniMax(_board, depth + 1, false);
						_board[i, j] = 0;
						bestScore = Math.Max(score, bestScore);
						// if (score > bestScore)
						// {
						// 	bestScore = score;
						// }						
					}
				}
			}
			return bestScore;
			 // iswinner draw == 0, x == 1, o == 2
		}
		else
		{
			int bestScore = 1000;
			for (int i = 0; i <= _board.GetUpperBound(0); i++)
			{
				for (int j = 0; j <= _board.GetUpperBound(1); j++)
				{
					if (_board[i, j] == 0)
					{
						_board[i, j] = _humanPlayer;
						score = MiniMax(_board, depth + 1, true);
						_board[i, j] = 0;
						bestScore = Math.Min(score, bestScore);
						// if (score < bestScore)
						// {
						// 	bestScore = score;
						// }						
					}
				}
			}
			return bestScore;
		}
	}

	public void SetAIMove(int i, int j)
	{
		Tuple<int, int> coord = new Tuple<int, int>(i, j);
		SetBoardValue(coord, _computerPlayer);
		SetButtonText(_btnBoardDict.ToDictionary(x => x.Value, x => x.Key)[coord], _computerPlayer);
	}

	public void OnButtonPressed(string btnName)
	{
		if (_currentTurn == _computerPlayer || _currentTurn == 0)
		{
			return;
		}
		if (GetBoardValue(_btnBoardDict[btnName]) != 0)
		{
			return;
		}

		SetBoardValue(_btnBoardDict[btnName], _humanPlayer);
		SetButtonText(btnName, _humanPlayer);
		NextTurn();
	}

	public int IsWinner()
	{
		int winner = 0;
		
		for (int i = 0; i <= 2; i++)
		{
			if (CheckEquivalentRow(i) != 0)
			{
				winner = CheckEquivalentRow(i);
			}
		}
		for (int i = 0; i <= 2; i++)
		{
			if (CheckEquivalentCol(i) != 0)
			{
				winner = CheckEquivalentCol(i);
			}
		}

		winner = CheckEquivalentDiag() != 0 ? CheckEquivalentDiag() : winner;

		return winner;
	}

	public bool IsStalemate()
	{
		for (int i = 0; i <= _board.GetUpperBound(0); i++)
		{
			for (int j = 0; j <= _board.GetUpperBound(1); j++)
			{
				if (_board[i, j] == 0)
				{
					return false;
				}
			}
		}
		return true;
	}

	public int CheckEquivalentRow(int row)
	{
		if (GetBoardValue(row, 0) == GetBoardValue(row, 1) && GetBoardValue(row,1) == GetBoardValue(row, 2))
		{
			return GetBoardValue(row, 0);
		}
		return 0;
	}

	public int CheckEquivalentCol(int col)
	{
		if (GetBoardValue(0, col) == GetBoardValue(1, col) && GetBoardValue(1,col) == GetBoardValue(2, col))
		{
			return GetBoardValue(0, col);
		}
		return 0;
	}
	public int CheckEquivalentDiag()
	{
		if (GetBoardValue(0, 0) == GetBoardValue(1, 1) && GetBoardValue(1,1) == GetBoardValue(2, 2))
		{
			return GetBoardValue(0, 0);
		}
		if (GetBoardValue(2, 0) == GetBoardValue(1, 1) && GetBoardValue(1,1) == GetBoardValue(0, 2))
		{
			return GetBoardValue(2, 0);
		}
		return 0;
	}

	public void SetButtonText(string btnName, int num)
	{
		_panel.GetNode<Label>(btnName + "/Label").Text = num == 1 ? "x" : "o";
	}

	public void SetBoardValue(Tuple<int, int> coord, int num)
	{
		_board[coord.Item1, coord.Item2] = num;
	}
	public void SetBoardValue(int x, int y, int num)
	{
		_board[x, y] = num;
	}

	public int GetBoardValue(Tuple<int, int> coord)
	{
		return _board[coord.Item1, coord.Item2];
	}

	public int GetBoardValue(int x, int y)
	{
		return _board[x, y];
	}
	
	private void OnBtnClosePressed()
	{
		_particlesBread.Emitting = false;
		// if (GetNode<AnimationPlayer>("AnimStart") != null)
		// {
		GetNode<AnimationPlayer>("AnimStart").Play("Disappear");
		AudioHandler.PlaySound(_soundPlayer, _noughtsSounds[AudioData.NoughtsSounds.End], AudioData.SoundBus.Effects);
		// }
		// Hide();
	}

	private void OnBtnResetPressed()
	{
		Reset();
	}



	private void OnFireworksTimerTimeout()
	{
		_particlesBread.Emitting = false;
	}

}

