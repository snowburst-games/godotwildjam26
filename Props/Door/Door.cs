using Godot;
using System;
using System.Collections.Generic;

public class Door : TextureButton
{
	
	[Export]
	public LevelManager.Level TargetLevel {get; set;} = LevelManager.Level.Office;
	[Export]
	private string _infoText = "";

	// private Light2D _light;
	private Label _lbl;
	private InfoPanel _infoPanel;
	private Sprite _sprite;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.DoorSounds, AudioStream> _doorSounds = AudioData.LoadedSounds<AudioData.DoorSounds>(AudioData.DoorSoundPaths);
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// _light = GetNode<Light2D>("Light2D");
		_sprite = GetNode<Sprite>("Sprite");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		// _light.Visible = false;
		_infoPanel = GetNode<InfoPanel>("InfoPanel");
		// Connect("pressed", this, nameof(Test), new Godot.Collections.Array {TargetLevel});
		_lbl = GetNode<Label>("Label");
		_lbl.Text = "Door to " + TargetLevel.ToString();
		_infoPanel.Init(_infoText);
	}

	private void OnMouseEntered()
	{
		// _light.Visible = true;
		_sprite.Modulate = new Color(1.5f,1.5f,1.5f);
	}


	private void OnMouseExited()
	{
		// _light.Visible = false;
		_sprite.Modulate = new Color(1,1,1);
	}
	
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	// public void Test(LevelManager.Level targetLevel)
	// {
	// 	GD.Print(targetLevel.ToString());
	// }
	private void OnBtnSpriteGuiInput(InputEvent ev)
	{
		if (ev is InputEventMouseButton evb)
		{
			if (evb.Pressed)
			{
				if (TargetLevel != LevelManager.Level.HR)
				{
					return;
				}
				if (evb.ButtonIndex == (int)ButtonList.Right)
				{
					_infoPanel.Init(_infoText);
					_infoPanel.Start();
					_infoPanel.Visible = true;
				}
			}
			else
			{
				_infoPanel.Visible = false;
			}
		}
	}

	public void OnDoorOpened()
	{
		// if (TargetLevel != LevelManager.Level.HR)
		// {
		// 	return;
		// }
		AudioHandler.PlaySound(_soundPlayer, _doorSounds[AudioData.DoorSounds.Open], AudioData.SoundBus.Effects);
	}

}
