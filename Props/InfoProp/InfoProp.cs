using Godot;
using System;
using System.Collections.Generic;

public class InfoProp : TextureButton
{
	
	[Export]
	private string _infoText = "";

	[Export]
	private List<string> _infoTextList = new List<string>();

	// private Light2D _light;
	private InfoPanel _infoPanel;
	private Sprite _sprite;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// _light = GetNode<Light2D>("Light2D");
		// _light.Visible = false;
		_infoPanel = GetNode<InfoPanel>("InfoPanel");
		_sprite = GetNode<Sprite>("Sprite");

		if (_infoTextList.Count > 0)
		{
			_infoText = "";
			for (int i = 0; i < _infoTextList.Count; i++)
			{
				_infoText += _infoTextList[i];
				if (i != _infoTextList.Count-1)
				{
					_infoText += "\n";
				}
			}
		}

		_infoPanel.Init(_infoText);
	}

	private void OnMouseEntered()
	{
		// _light.Visible = true;
		_sprite.Modulate = new Color(1.5f,1.5f,1.5f);
	}


	private void OnMouseExited()
	{
		// _light.Visible = false;
		_sprite.Modulate = new Color(1,1,1);
	}
	
	private void OnBtnSpriteGuiInput(InputEvent ev)
	{
		if (ev is InputEventMouseButton evb)
		{
			if (evb.Pressed)
			{
				// if (evb.ButtonIndex == (int)ButtonList.Right)
				{
					_infoPanel.Init(_infoText);
					_infoPanel.Start();
					_infoPanel.Visible = true;
				}
			}
			else
			{
				_infoPanel.Visible = false;
			}
		}
	}
}
