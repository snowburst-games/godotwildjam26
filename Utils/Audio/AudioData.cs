// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};


	public enum ButtonSounds {
		ClickStart
	};


	public enum MainMenuSounds {
		Music,
		Intro
	};
	public enum WorldSounds {
		Music,
		Paper,
		Accuse,
		Win
	};

	public enum RoomSounds {
		
	};

	public enum DoorSounds {
		Open
	}
	public enum NoughtsSounds {
		Start,
		End,
		CheatVictory
	}

	public enum CharacterSounds {
		MaleGreeting1,
		MaleGreeting2,
		MaleGreeting3,
		FemaleGreeting1,
		FemaleGreeting2,
		FemaleGreeting3
	}

	public enum CompanionSounds {
		Test1,
		Test2
	}

	public static Dictionary<ButtonSounds, string> ButtonSoundPaths = new Dictionary<ButtonSounds, string>()
	{
		{ButtonSounds.ClickStart, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<MainMenuSounds, string> MainMenuSoundPaths = new Dictionary<MainMenuSounds, string>()
	{
		{MainMenuSounds.Music, "res://Stages/MainMenu/Music/SpookyOffice_Menu3.ogg"},
		{MainMenuSounds.Intro, "res://Stages/MainMenu/PictureStory/Intro.wav"},
	};

	public static Dictionary<WorldSounds, string> WorldSoundPaths = new Dictionary<WorldSounds, string>()
	{
		{WorldSounds.Music, "res://Stages/World/Music/SpookyGoingOn_world.ogg"},
		{WorldSounds.Paper, "res://Stages/World/HUD/Journal/PageTurn.wav"},
		{WorldSounds.Accuse, "res://Stages/World/HUD/Accuse/MakeAccusation.wav"},
		{WorldSounds.Win, "res://Stages/World/Sounds/SpookyBreadWin.wav"}
	};


	public static Dictionary<RoomSounds, string> RoomSoundPaths = new Dictionary<RoomSounds, string>()
	{
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};
	public static Dictionary<DoorSounds, string> DoorSoundPaths = new Dictionary<DoorSounds, string>()
	{
		{ DoorSounds.Open, "res://Props/Door/Creak.wav"}
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<NoughtsSounds, string> NoughtsSoundPaths = new Dictionary<NoughtsSounds, string>()
	{
		{ NoughtsSounds.Start, "res://Props/NoughtsCrossesGame/TicTacToeStart.wav"},
		{ NoughtsSounds.End, "res://Props/NoughtsCrossesGame/TicTacToeEnd.wav"},
		{ NoughtsSounds.CheatVictory, "res://Props/NoughtsCrossesGame/WinTicTacToev0.1.wav"},
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};
	public static Dictionary<CompanionSounds, string> CompanionSoundPaths = new Dictionary<CompanionSounds, string>()
	{
		{ CompanionSounds.Test1, "res://Stages/World/Sounds/placeholder.wav"},
		{ CompanionSounds.Test2, "res://Stages/World/Sounds/placeholder.wav"}
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<CharacterSounds, string> CharacterSoundPaths = new Dictionary<CharacterSounds, string>()
	{
		{ CharacterSounds.MaleGreeting1, "res://Actors/Character/Sounds/MaleGreeting1.wav"},
		{ CharacterSounds.MaleGreeting2, "res://Actors/Character/Sounds/MaleGreeting2.wav"},
		{ CharacterSounds.MaleGreeting3, "res://Actors/Character/Sounds/MaleGreeting3.wav"},
		{ CharacterSounds.FemaleGreeting1, "res://Actors/Character/Sounds/FemaleGreeting1.wav"},
		{ CharacterSounds.FemaleGreeting2, "res://Actors/Character/Sounds/FemaleGreeting2.wav"},
		{ CharacterSounds.FemaleGreeting3, "res://Actors/Character/Sounds/FemaleGreeting3.wav"}
	};

	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
