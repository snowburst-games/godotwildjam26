using Godot;
using System;

public class PnlAccuse : Panel
{
	public delegate void CharacterAccusedDelegate(DialogueControl.CharacterEnum character, bool guilty);
	public event CharacterAccusedDelegate CharacterAccused;

	private GridContainer _grid;
	private DialogueControl.CharacterEnum _guilty = DialogueControl.CharacterEnum.Robin;
	private DialogueControl.CharacterEnum _accusedChar;
	private Control _cntConfirm;
	private Button _btnConfirm;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_grid = GetNode<GridContainer>("Grid");
		_cntConfirm = GetNode<Control>("CntConfirm");
		_btnConfirm = GetNode<Button>("BtnConfirm");
		OnBtnBackPressed();

		foreach (string name in Enum.GetNames(typeof(DialogueControl.CharacterEnum)))
		{
			Button b = new Button();
			_grid.AddChild(b);
			b.Text = name;
			b.RectMinSize = new Vector2(166, 30);
			b.Connect("pressed", this, nameof(OnBtnCharacterPressed), new Godot.Collections.Array {name});
		}
	}

	public void DisableOneButton(string name)
	{
		foreach (Node n in _grid.GetChildren())
		{
			if (n is Button button)
			{
				if (button.Text == name)
				{
					button.Disabled = true;
				}
				else
				{
					button.Disabled = false;
				}
			}
		}
	}

	public void OnBtnCharacterPressed(string name)
	{
		_accusedChar = (DialogueControl.CharacterEnum) Enum.Parse(typeof(DialogueControl.CharacterEnum), name, true);
		DisableOneButton(name);
		_btnConfirm.Text = "You accuse " + name +". Confirm?";
		_btnConfirm.Visible = true;
	}

	private void OnBtnAccusePressed()
	{
		Visible = true;
	}


	private void OnBtnConfirmPressed()
	{
		// _cntConfirm.Visible = true;
		CharacterAccused?.Invoke(_accusedChar, _accusedChar == _guilty);
	}

	private void OnBtnBackPressed()
	{
		_cntConfirm.Visible = false;
		_btnConfirm.Visible = false;
		Visible = false;
		DisableOneButton("");
	}

	private void OnBtnNoPressed()
	{
		_cntConfirm.Visible = false;
	}


	private void OnBtnYesPressed()
	{
		CharacterAccused?.Invoke(_accusedChar, _accusedChar == _guilty);
	}

	public override void _Input(InputEvent ev)
	{
		if (Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > RectGlobalPosition.x && evMouseButton.Position.x < RectSize.x + RectGlobalPosition.x
			&& evMouseButton.Position.y > RectGlobalPosition.y && evMouseButton.Position.y < RectSize.y + RectGlobalPosition.y) )
			{
				OnBtnBackPressed();
			}
		}
	}
	
}



