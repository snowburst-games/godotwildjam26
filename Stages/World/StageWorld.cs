using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{

	private DialogueControl _dialogueControl;
	private StoryManager _storyManager;
	private PnlJournal _pnlJournal;
	private Companion _companion;
	private PnlAccuse _pnlAccuse;
	private AudioStreamPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.WorldSounds, AudioStream> _worldSounds = AudioData.LoadedSounds<AudioData.WorldSounds>(AudioData.WorldSoundPaths);

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_dialogueControl = GetNode<DialogueControl>("LevelManager/DialogueControl");
		_pnlJournal = GetNode<PnlJournal>("HUD/PnlJournal");
		_pnlAccuse = GetNode<PnlAccuse>("HUD/PnlAccuse");
		_storyManager = GetNode<StoryManager>("HUD/StoryManager");
		_companion = GetNode<Companion>("HUD/Companion");
		_dialogueControl.JournalUpdated+=_pnlJournal.OnJournalUpdated;
		// _dialogueControl.CharacterAccused+=_storyManager.OnCharacterAccused;
		_pnlAccuse.CharacterAccused+=_storyManager.OnCharacterAccused;
		_dialogueControl.CompanionReacts+=_companion.StartSpeech;
		_storyManager.EndStoryFinished+=GoToMainMenu;
		GetNode<Control>("HUD/CntConfirmExit").Visible = false;
		

		// _storyManager.OnCharacterAccused(DialogueControl.CharacterEnum.Dwight, false); // testing only
	}

	
	public override void Init()
	{
		base.Init();

		AudioHandler.PlaySound(_musicPlayer, _worldSounds[AudioData.WorldSounds.Music], AudioData.SoundBus.Music);
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }

	public void DoPaperSound()
	{
		AudioHandler.PlaySound(_soundPlayer, _worldSounds[AudioData.WorldSounds.Paper], AudioData.SoundBus.Effects);
	}

	public void DoAccuseSound()
	{
		AudioHandler.PlaySound(_soundPlayer, _worldSounds[AudioData.WorldSounds.Accuse], AudioData.SoundBus.Effects);
	}

	public void GoToMainMenu()
	{
		GetNode<Control>("HUD/CntConfirmExit").Visible = false;
		_dialogueControl.Die();
		_storyManager.Die();
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}
	private void OnBtnMainMenuPressed()
	{
		GetNode<Control>("HUD/CntConfirmExit").Visible = true;
	}
	private void OnConfirmExitBtnNoPressed()
	{
		GetNode<Control>("HUD/CntConfirmExit").Visible = false;
	}
}


